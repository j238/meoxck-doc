# MEOリポートシステム手順書

## 管理者ログイン/ユーザー作成
1. 管理者アカウントでログイン  
1. 管理者画面からユーザー作成  
1. 社名を入力  
1. ユーザー追加すると、登録したユーザーは  
社名  
メールアドレス/パスワード  
で表示されます。  
このメールアドレス/パスワードでユーザーはログインできます。  
ユーザーにこのメールアドレス/パスワードをお知らせください。  

## キーワード画面/キーワードを追加

1. キーワードを追加を押下  
1. キーワード登録ダイアログで  
店名: google mapで登録している名前と一緒にしてください。  
この名前と一致するかどうかで順位を判断するので。  
キーワード: 検索したいキーワード  
ズーム: マップのズーム値(デフォルト:14)    
ズーム値の詳細は下記参照  
https://qiita.com/SnowMonkey/items/795779913be692c12a0b  
デバイス:  
iPhone X  
iPhone 8  
iPhone 8 Plus  
iPad  
PC  
指定したデバイスでChromeで検索した場合と一緒の結果が得られるようになります。  
PC以外は画面サイズ（幅、高さ）はそのデバイスに固定になりますので指定できません。  
iPadはiPad Pro/Miniと一緒の設定になります。  
iPhone 8/8 Plus/Xは画面サイズがそれぞれ違います。  
また、iPhone 8/8 Plus/X（スマホ）は地図と店舗リストを同時に表示できませんので、店舗リストのみをスクショに取ります。  
検索内容をキーワードに反映させるためにはコツが要るので解説します。  
    * 手順  
    1. デバイス(例えばiPhone 8とします)のChromeで https://www.google.co.jp/maps を表示  
    1. 検索します(例えば 「塚本駅 ラーメン」とします)  
    1. 納得できる検索結果ができたならURLを共有などを使ってコピーします（クリップボードへコピーやGMailで共有など）  
    1. URLが下記だった場合  
https://www.google.co.jp/maps/search/%E5%A1%9A%E6%9C%AC%E9%A7%85+%E3%83%A9%E3%83%BC%E3%83%A1%E3%83%B3/@34.7137488,135.4713291,15z  
(+はスペースを意味するのでスペースに読み替えてください)  
本ツールのキーワード設定のキーワードにデバイスでの検索時と同じキーワード（例では「塚本駅 ラーメン」）を設定します。  
@の後の34.7137488が緯度になりますので、そのままの数値を緯度に設定します。  
その後の135.4713291が経度になりますので、そのままの数値を経度に設定します。  
その後の15zがズームになりますので、15をズームに設定します。（zは含めないでください）  
デバイスを検索したデバイスに設定します(この例ではiPhone 8)  
これで本ツールで全く同じ検索結果が得られます。  

    緯度: 検索する座標  
    経度: 検索する座標  
    検索住所: 検索住所名を入力  
    （「住所から経緯度を検索」で検索住所から経緯度を検索することができます）  
    上記を入力したら決定押下  

## キーワード画面/スケジュール登録
1. スケジュール登録押下で自動検索のスケジュールを設定できます  
1. 毎日の実行時間をしてください  

## 検索スケジュール画面
1. キーワード画面で登録したスケジュール一覧を表示できます  
1. 削除ボタン押下で削除できます

## チャート画面/検索
1. 検索を押下で即時検索できます。

## チャート画面/データ追加
1. データ追加を押下で手動でデータを追加できます
1. キーワードを選択して次へ  
1. 実行時間を入力して次へ  
1. 順位を入力して決定  

## チャート画面/テストデータの作成
テスト用のデータを適当に作ります。  
（ほぼ当方のテスト用機能なので使うことはないかと思います）  

## ログアウト
1. 管理者画面の左上のメニューボタン押下  
1. メニューからログアウト押下